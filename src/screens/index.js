import Home from './Home'
import DetailScreen from './DetailScreen'
import Splash from './Splash'

export{Home, DetailScreen, Splash}