import React from 'react';
import {Text, View, Dimensions, StyleSheet, Image} from 'react-native';
import {Rating} from 'react-native-ratings';
import { WARNA_UTAMA, WARNA_SEKUNDER } from '../utils/constant';

function MovieBox({
  image,
  title,
  tagline,
  status,
  runtime,
  vote_average,
  release_date,
}) {
  return (
    <View style={styles.container}>
      <Image source={{uri: `${image}`}} style={styles.poster} />
      <View style={{flex: 1, justifyContent: 'flex-start'}}>
        <View>
          <Text style={styles.bold}>{title}</Text>
        </View>
        <View>
          <View style={styles.info}>
            <Text style={styles.menuTitle}>Release Date</Text>
            <Text style={styles.menuContent}>{release_date}</Text>
          </View>
          <View style={styles.info}>
            <Text style={styles.menuTitle}>Tagline</Text>
            <Text style={styles.menuContent}>{tagline}</Text>
          </View>
          <View style={styles.info}>
            <Text style={styles.menuTitle}>Status</Text>
            <Text style={styles.menuContent}>{status}</Text>
          </View>
          <View style={styles.info}>
            <Text style={styles.menuTitle}>Runtime</Text>
            <Text style={styles.menuContent}>{runtime}</Text>
          </View>
          <View style={styles.info}>
            <Text style={styles.menuTitle}>Rating</Text>
            <View style={styles.rating}>
              <Rating
                ratingCount={10}
                tintColor={'black'}
                startingValue={vote_average}
                imageSize={20}
                readonly={true}
                style={styles.rating}
              />
              <Text style={styles.menuContent}>{vote_average}</Text>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
}

export default MovieBox;

const screens = Dimensions.get('screen');
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: WARNA_SEKUNDER,
    marginHorizontal: screens.width * 0.025,
    borderRadius: 12,
    marginTop: -screens.height * 0.1,
    width: screens.width * 0.95,
    flexDirection: 'row',
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    flexDirection: 'row',
  },
  kalimatAtas: {
    paddingTop: 17,
    paddingLeft: 17,
    fontSize: 18,
    color: 'white',
  },
  kalimatBawah: {
    paddingLeft: 17,
    fontSize: 18,
    color: 'white',
  },
  banner: {
    width: screens.width * 0.9,
    height: 130,
    flexDirection: 'column',
    alignItems: 'flex-start',
  },
  buttonAksi: {
    paddingTop: 17,
    paddingLeft: 17,
    paddingBottom: 17,
  },
  poster: {
    width: 140,
    height: 180,
    borderRadius: 20,
    resizeMode: 'contain',
  },
  menuTitle: {
    color: WARNA_UTAMA,
    fontWeight: 'bold',
    paddingRight: 5,
  },
  menuContent: {
    color: WARNA_UTAMA, 
  },
  bold: {
    fontWeight: 'bold',
    alignSelf: 'center',
    color: WARNA_UTAMA,
    fontSize: 25,
  },
  rating: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingRight: 10,
  },
  info: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-start',
  },
});
