import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {
  ActivityIndicator,
  Dimensions,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Alert,
} from 'react-native';
import {WARNA_UTAMA, WARNA_SEKUNDER} from '../utils/constant';

const Home = ({navigation}) => {
  const [movie, setmovie] = useState([]);
  const [Visibility, setVisibility] = useState(true);

  useEffect(() => {
    setTimeout(() => {
      setVisibility(!Visibility);
    }, 1000);
    axios
      .get('http://code.aldipee.com/api/v1/movies')
      .then(responseData => {
        setmovie(responseData.data.results);
      })
      .catch(err => {
        Alert.alert('ERROR MESSAGE', `${err}`);
      });
  }, []);
  return (
    <View style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <Text style={styles.menu}>Recommended</Text>
        <View>
          {Visibility && (
            <ActivityIndicator
              animating={Visibility}
              hidesWhenStopped={Visibility}
            />
          )}
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            {movie.map(item => {
              return (
                <View>
                  {!Visibility && (
                    <TouchableOpacity
                      onPress={() => {
                        navigation.navigate('DetailScreen', {
                          id: `${item.id}`,
                        });
                      }}>
                      <Image
                        style={styles.recommend}
                        source={{uri: `${item.poster_path}`}}
                      />
                    </TouchableOpacity>
                  )}
                </View>
              );
            })}
          </ScrollView>
        </View>

        <Text style={styles.menu}>Latest Upload</Text>
        {Visibility && (
          <ActivityIndicator
            animating={Visibility}
            hidesWhenStopped={Visibility}
          />
        )}
        <View style={{flex: 1}}>
          {movie.map(item => {
            return (
              <View>
                {!Visibility && (
                  <View style={styles.content}>
                    <Image
                      style={styles.latestUpload}
                      source={{uri: `${item.poster_path}`}}
                    />
                    <View style={{flex: 1, alignItems: 'flex-start'}}>
                      <Text style={styles.title}>{item.title}</Text>
                      <View style={{flexDirection: 'row'}}>
                        <Text style={styles.title2}>Release Date</Text>
                        <Text style={styles.detail}>{item.release_date}</Text>
                      </View>
                      <View style={{flexDirection: 'row'}}>
                        <Text style={styles.title2}>Rating</Text>
                        <Text style={styles.detail}>{item.vote_average}</Text>
                      </View>
                      <View style={{paddingLeft: 10}}>
                        <TouchableOpacity
                          style={styles.button}
                          onPress={() => {
                            navigation.navigate('DetailScreen', {
                              id: `${item.id}`,
                            });
                          }}>
                          <Text
                            style={{color: WARNA_UTAMA, fontWeight: 'bold'}}>
                            Show More
                          </Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                )}
              </View>
            );
          })}
        </View>
      </ScrollView>
    </View>
  );
};

export default Home;
const screen = Dimensions.get('screen');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: WARNA_UTAMA,
    padding: 10,
  },
  title: {
    color: WARNA_SEKUNDER,
    fontWeight: 'bold',
    fontSize: 20,
    paddingLeft: 10,
    flex: 1,
  },
  menu: {
    color: WARNA_SEKUNDER,
    paddingLeft: 10,
    fontSize: 20,
    paddingBottom: 5,
    fontWeight: 'bold',
  },
  title2: {
    color: WARNA_SEKUNDER,
    paddingLeft: 10,
    fontSize: 16,
    paddingBottom: 5,
    fontWeight: '900',
  },
  detail: {
    color: WARNA_SEKUNDER,
    paddingLeft: 10,
    fontSize: 16,
    paddingBottom: 5,
  },
  content: {
    flexDirection: 'row',
    paddingVertical: 10,
    paddingLeft: 10,
  },
  recommend: {
    width: 140,
    height: 180,
    borderRadius: 20,
    resizeMode: 'contain',
  },
  latestUpload: {
    width: 140,
    height: 140,
    borderRadius: 10,
    resizeMode: 'cover',
  },
  button: {
    paddingHorizontal: 15,
    paddingVertical: 10,
    backgroundColor: WARNA_SEKUNDER,
    borderRadius: 10,
  },
});
