import axios from 'axios';
import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  ImageBackground,
  ActivityIndicator,
  Dimensions,
  TouchableOpacity,
  Alert,
} from 'react-native';
import MovieBox from '../components/MovieBox';
import {Back, Like, Share} from '../assets';
import Shares from 'react-native-share';
import files from '../assets/Base64';
import {FORMAT_UKURAN, WARNA_SEKUNDER, WARNA_UTAMA} from '../utils/constant';

const DetailScreen = ({route, navigation}) => {
  const {id} = route.params;
  const [detailMovie, setdetailMovie] = useState([]);
  const [Cast, setCast] = useState([]);
  const [Genre, setGenre] = useState([]);
  const [Visibility, setVisibility] = useState(true);

  useEffect(() => {
    setTimeout(() => {
      setVisibility(!Visibility);
    }, 1000);
    axios
      .get(`http://code.aldipee.com/api/v1/movies/${id}`)
      .then(responseData => {
        setdetailMovie(responseData.data);
        setCast(responseData.data.credits.cast);
        setGenre(responseData.data.genres);
      })
      .catch(err => {
        Alert.alert('ERROR MESSAGE', `${err}`);
      });
  }, []);
  return (
    <View style={styles.container}>
      {Visibility && (
        <View>
          <ActivityIndicator
            animating={Visibility}
            hidesWhenStopped={Visibility}
            size={100}
          />
        </View>
      )}
      {!Visibility && (
        <ScrollView showsVerticalScrollIndicator={false}>
          <ImageBackground
            source={{uri: `${detailMovie.backdrop_path}`}}
            style={styles.header}>
            <View style={styles.buttonContainer}>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('Home');
                }}>
                <Back />
              </TouchableOpacity>
              <View style={{flexDirection: 'row'}}>
                <TouchableOpacity
                  onPress={() => {
                    Alert.alert('Anda telah menyukai film ini');
                  }}>
                  <Like />
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => {
                    const shareOptions = {
                      title: `${detailMovie.title}`,
                      url: files.image1,
                    };

                    try {
                      Shares.open(shareOptions);
                    } catch (err) {
                      console.log('ERROR:', err);
                    }
                  }}>
                  <Share />
                </TouchableOpacity>
              </View>
            </View>
          </ImageBackground>
          <MovieBox
            image={detailMovie.poster_path}
            title={detailMovie.title}
            tagline={detailMovie.tagline}
            status={detailMovie.status}
            runtime={detailMovie.runtime}
            vote_average={detailMovie.vote_average}
            release_date={detailMovie.release_date}
          />
          <View>
            <View>
              <Text style={styles.menu}>Genres</Text>
              <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}>
                {Genre.map(item => {
                  return (
                    <View
                      style={{
                        alignSelf: 'center',
                        backgroundColor: WARNA_SEKUNDER,
                        margin: 10,
                        paddingHorizontal: 10,
                        borderRadius: 10,
                      }}>
                      <Text style={styles.genre}>{item.name}</Text>
                    </View>
                  );
                })}
              </ScrollView>
            </View>
            <View>
              <Text style={styles.menu}>Synopsis</Text>
              <Text style={styles.sinopsis}>{detailMovie.overview}</Text>
            </View>

            <View>
              <Text style={styles.menu}>Casts</Text>
              <ScrollView horizontal={true}>
                {Cast.map(item => {
                  return (
                    <View
                      style={{
                        flex: 1,
                        backgroundColor: WARNA_SEKUNDER,
                        alignItems: 'center',
                        justifyContent: 'center',
                        margin: 10,
                        paddingVertical: 10,
                        paddingHorizontal: 10,
                        borderRadius: 10,
                        width: 175,
                      }}>
                      <View style={{flex: 3}}>
                        <Image
                          source={{
                            uri: `${item.profile_path}`,
                          }}
                          style={styles.poster}
                        />
                      </View>
                      <View style={{flex: 1}}>
                        <Text style={styles.castname}>{item.name}</Text>
                      </View>
                    </View>
                  );
                })}
              </ScrollView>
            </View>
          </View>
        </ScrollView>
      )}
    </View>
  );
};

export default DetailScreen;

const screens = Dimensions.get('screen');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: WARNA_UTAMA,
    justifyContent: 'center',
  },
  header: {
    width: screens.width,
    height: screens.height * 0.25,
    alignItems: 'center',
  },
  menu: {
    color: WARNA_SEKUNDER,
    paddingLeft: 10,
    fontSize: 20,
    paddingTop: 10,
    paddingBottom: 5,
    fontWeight: 'bold',
  },
  text: {
    color: 'white',
    fontSize: FORMAT_UKURAN,
  },
  genre: {
    color: WARNA_UTAMA,
    fontSize: FORMAT_UKURAN,
  },
  sinopsis: {
    color: 'white',
    fontSize: 20,
    textAlign: 'justify',
    paddingHorizontal: 10,
  },
  poster: {
    width: 150,
    height: 150,
    resizeMode: 'cover',
  },
  castname: {
    color: WARNA_UTAMA,
    fontSize: 20,
    textAlign: 'center',
  },
  button: {
    width: 100,
    height: 100,
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 50,
    width: screens.width * 0.95,
  },
});
